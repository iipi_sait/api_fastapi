from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy

Base = declarative_base()

class Lang(Base):
    __tablename__ = 'langs'

    id = Column(Integer, primary_key=True, index=True)
    short = Column(String,unique=True,index=True)
    name = Column(String)
