from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy

Base = declarative_base()

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String,unique=True,index=True)
    name = Column(String)
    hashed_password=Column(String)
    is_admin= Column(sqlalchemy.Boolean())
    is_active = Column(sqlalchemy.Boolean())
    departgroupid  = Column(Integer,  index=True)
    type_user = Column(Integer)
