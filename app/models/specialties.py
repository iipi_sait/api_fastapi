from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy

Base = declarative_base()

class Specialties(Base):
    __tablename__ = 'specialties'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    slug = Column(String)
    direction = Column(String)
    profile = Column(String)
    period_development = Column(String)
    form_education = Column(String)
    is_dogovor = Column(sqlalchemy.Boolean())
    is_pyument = Column(sqlalchemy.Boolean())
    about = Column(String, nullable=True)