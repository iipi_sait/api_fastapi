from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy

Base = declarative_base()

class Debartgroup(Base):
    __tablename__ = 'departgroup'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String,unique=True,index=True)
    about = Column(sqlalchemy.Text())
