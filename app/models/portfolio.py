from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from .user import User
import sqlalchemy

Base = declarative_base()

class Portfolio(Base):
    __tablename__ = 'portfolio'

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, index=True)
    type = Column(Integer, index=True)
    vid = Column(String)
    name = Column(String)
    point = Column(String)
    datarecord = Column(sqlalchemy.DateTime)
    about = Column(sqlalchemy.Text())
