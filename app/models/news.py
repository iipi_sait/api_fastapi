from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy

Base = declarative_base()

class News(Base):
    __tablename__ = 'news'

    id = Column(Integer, primary_key=True, index=True)
    slug = Column(String,unique=True,index=True)
    name = Column(String)
    text = Column(sqlalchemy.Text())
    categori_id = Column(Integer)
    photo = Column(String)
    datapublic = Column(sqlalchemy.DateTime)
    text_big = Column(sqlalchemy.Text())
