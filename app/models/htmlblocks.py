from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy

Base = declarative_base()

class Htmlblocks(Base):
    __tablename__ = 'htmlblocks'

    id = Column(Integer, primary_key=True, index=True)
    slug = Column(String,unique=True,index=True)
    name = Column(String)
    text = Column(sqlalchemy.Text())
