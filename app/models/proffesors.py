from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy

Base = declarative_base()

class Proffesor(Base):
    __tablename__ = 'professors'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    surname = Column(String, nullable=True)
    middle_name = Column(String, nullable=True)
    post = Column(String)
    photo = Column(String, nullable=True)
    sort = Column(Integer)
    about = Column(String, nullable=True)
    slug = Column(String,unique=True,index=True)
    inf_1 = Column(String, unique=True, index=True)
    inf_2 = Column(String, unique=True, index=True)
    inf_3 = Column(String, unique=True, index=True)
    inf_4 = Column(String, unique=True, index=True)
    inf_5 = Column(String, unique=True, index=True)
    inf_6 = Column(String, unique=True, index=True)
    about_text = Column(String, unique=True, index=True)
