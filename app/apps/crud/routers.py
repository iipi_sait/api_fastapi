from fastapi import APIRouter, Depends, File, UploadFile, Form
from fastapi.security import OAuth2PasswordBearer

from app.apps.crud.views import CrudView
from app.database import SessionLocal
from jose import jwt
from app.apps.auth.views import SECRET_KEY,ALGORITHM



router = APIRouter()
crud_views = CrudView(db=SessionLocal)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

async def get_user_from_token(token: str = Depends(oauth2_scheme)):
    try:
       payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])  # декодируем токен
       return payload.get("sub")  # тут мы идем в полезную нагрузку JWT-токена и возвращаем утверждение о юзере (subject); обычно там еще можно взять "iss" - issuer/эмитент, или "exp" - expiration time - время 'сгорания' и другое, что мы сами туда кладем
    except jwt.ExpiredSignatureError:
       pass  # тут какая-то логика ошибки истечения срока действия токена
    except jwt.InvalidTokenError:
       pass




@router.post("/list/{tablename}")
async def crud_show_datas(tablename: str,current_user: str = Depends(get_user_from_token)):
    datas=crud_views.get_list_datas(tablename,current_user)
    return {"table_datas":datas}

@router.post("/show/{tablename}/{id}")
async def crud_show_data_id(tablename: str,id:int,current_user: str = Depends(get_user_from_token)):
    DataRecord=crud_views.get_show_data_id(tablename,current_user,id)
    return {"record_data":DataRecord}

@router.post("/store/{tablename}")
async def crud_store_data_id(tablename: str,data:dict,current_user: str = Depends(get_user_from_token)):
    print(tablename)
    DataRecord=crud_views.store_data_id(tablename,data,current_user)
    return {"record_data":DataRecord}

@router.post("/update/{tablename}")
async def crud_update_data_id(tablename: str,data:dict,current_user: str = Depends(get_user_from_token)):
    DataRecord=crud_views.update_data_id(tablename,data,current_user)
    return {"record_data":DataRecord}

@router.post("/delete/{tablename}")
async def crud_update_data_id(tablename: str,data:dict,current_user: str = Depends(get_user_from_token)):
    DataRecord=crud_views.delete_data_id(tablename,data['id'],current_user)
    return {"msg":DataRecord}

@router.post("/{tablename}/fileupload/")
async def crud_update_file_data_id(tablename,
                                    recordid: str = Form(...),
                                    column: str = Form(...),
                                    file: UploadFile = File(...),
                                    current_user: str = Depends(get_user_from_token)):
    DataRecord=crud_views.update_file_data(tablename,
                                           recordid,
                                           column,
                                           file,
                                           current_user)
    return {"msg":DataRecord}