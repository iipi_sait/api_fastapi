from fastapi import HTTPException, File, UploadFile
from fastapi.responses import JSONResponse
from datetime import datetime
from sqlalchemy.orm import Session
from app.models.user import User
from app.models.lang import Lang
from app.models.specialties import  Specialties
from app.models.proffesors import Proffesor
from app.models.categories import Categories
from app.models.news import News
from app.models.htmlblocks import Htmlblocks
from app.models.departgroup import Debartgroup
from app.models.portfolio import Portfolio

import os
import shutil

class CrudView:

    def __init__(self, db: Session):
        self.db = db

    #Проверка пользователя в БД и являеться ли он администратором
    def getIsAdmin(self,UserEmail):
        db_user = self.db.query(User).filter(User.email == UserEmail).first()
        self.db.commit()
        if db_user is None:
            raise HTTPException(status_code=404, detail="User not found")
        if (db_user.is_admin):
            return db_user
        raise HTTPException(status_code=404, detail="Not admin")

    #по названию табилцы получаем модель к этой таблицы
    def getTableObject(self,NameTable):
        match NameTable:
            case "langs":
                objModel = Lang
            case "users":
                objModel = User
            case 'specialties':
                objModel = Specialties
            case 'professors':
                objModel = Proffesor
            case 'categories':
                objModel = Categories
            case 'news':
                objModel = News
            case 'htmlblocks':
                objModel = Htmlblocks
            case 'departgroup':
                objModel = Debartgroup
            case _:
                raise HTTPException(status_code=404, detail="Not table")
        return objModel

    # Выводим все данные этой таблицы
    def get_list_datas(self,NameTable:str,UserEmail:str):
        #Проверка авторизациия и есть статус администратора
        self.getIsAdmin(UserEmail)
        # Получаем объект
        objModel=self.getTableObject(NameTable)
        # Выводим все данные по объекту
        db_datas = self.db.query(objModel).all()
        # Возврошаем результат
        return db_datas

    # Выводим запись по ID  из  этой таблицы
    def get_show_data_id(self,NameTable:str,UserEmail:str,id:int):
        # Проверка авторизациия и есть статус администратора
        self.getIsAdmin(UserEmail)
        # Получаем объект
        objModel = self.getTableObject(NameTable)
        # Выводим все данные по ID
        db_data = self.db.query(objModel).filter(objModel.id == id).first()
        if db_data is None:
            raise HTTPException(status_code=404, detail="Not record id in table:"+NameTable)
        # Возврошаем результат
        return  db_data;

    # Новая запись
    def store_data_id(self,NameTable:str,datas:dict,UserEmail:str):
        # Проверка авторизациия и есть статус администратора
        self.getIsAdmin(UserEmail)
        # Получаем объект
        objModel = self.getTableObject(NameTable)
        try:
            NewRecorcd = objModel(**datas)
            self.db.add(NewRecorcd)
            self.db.commit()
            self.db.refresh(NewRecorcd)
        except Exception as e:
            self.db.rollback()
            raise HTTPException(status_code=500, detail="Failed  Table:"+NameTable+" Msg :"+ str(e))
        return NewRecorcd

    #Обновление записи
    def update_data_id(self,NameTable:str,datas:dict,UserEmail:str):
        # Проверка авторизациия и есть статус администратора
        self.getIsAdmin(UserEmail)
        # Получаем объект
        objModel = self.getTableObject(NameTable)
        db_record = self.db.query(objModel).filter(objModel.id == datas['id']).first()
        try:
            if db_record is None:
                raise HTTPException(status_code=404, detail="User not found")
            for key in datas:
                setattr(db_record, key, datas[key])
            self.db.commit()
            self.db.refresh(db_record)
        except Exception as e:
            self.db.rollback()
            raise HTTPException(status_code=500, detail="Failed  Table:"+NameTable+" Msg :"+ str(e))
        return db_record

    #Удалние записи
    def delete_data_id(self,NameTable:str,id:int,UserEmail:str):
        # Проверка авторизациия и есть статус администратора
        self.getIsAdmin(UserEmail)
        objModel = self.getTableObject(NameTable)
        db_record = self.db.query(objModel).filter(objModel.id == id).first()
        if db_record is None:
            raise HTTPException(status_code=404, detail="not found")
        self.db.delete(db_record)
        self.db.commit()
        return {id}

    def update_file_data(self,tablename: str,
                         recordid: int,
                         column: str,
                         file,
                         UserEmail: str):
        self.getIsAdmin(UserEmail)
        try:
            # Specify the directory where you want to save the uploaded files
            UPLOAD_DIR = "/code/app/images/" + tablename
            # Create the directory if it doesn't exist
            os.makedirs(UPLOAD_DIR, exist_ok=True)
            timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
            file_extension = os.path.splitext(file.filename)
            # Concatenate the upload directory and the filename
            newName = timestamp + file_extension[1]
            file_path = os.path.join(UPLOAD_DIR, newName)
            # Save the file to the specified directory
            with open(file_path, "wb") as f:
                shutil.copyfileobj(file.file, f)
                # Получаем объект
                objModel = self.getTableObject(tablename)
                db_record = self.db.query(objModel).filter(objModel.id == recordid).first()
                if db_record is None:
                    raise HTTPException(status_code=404, detail="REcord not found")
                setattr(db_record, column ,newName)
                self.db.commit()
                self.db.refresh(db_record)
                print('updateifle')
            return newName
        except Exception as e:
            return None