from fastapi import HTTPException
from sqlalchemy.orm import Session
from passlib.context import CryptContext
from jose import jwt

from app.models.user import User
from dotenv import load_dotenv
from pathlib import Path
import os

env_path =  '/code/app/.env'
load_dotenv(dotenv_path=env_path)

DATABASE_URL = os.getenv('SECRET_KEY')


# Configure JWT settings
SECRET_KEY = os.getenv('SECRET_KEY')
ALGORITHM = os.getenv('ALGORITHM')
#генерируем токен
def create_jwt_token(data: dict):
    return jwt.encode(data, SECRET_KEY, algorithm=ALGORITHM)
# Хешируем пароль
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
def hash_pass(password:str):
    return pwd_context.hash(password)


class AuthView:
    #Конструктор класса
    def __init__(self, db: Session):
        self.db = db

    # Вход в систему
    def login(self,UserEmail:str,UserPassword:str):
        DbUSer = self.db.query(User).filter(User.email == UserEmail).first()
        self.db.commit()
        if DbUSer is None:
            raise HTTPException(status_code=404, detail="Not found record")
        is_verified = pwd_context.verify(UserPassword, DbUSer.hashed_password)
        if is_verified:
            return {"token": create_jwt_token({"sub": DbUSer.email}), "token_type": "bearer"}
        else:
            raise HTTPException(status_code=500,detail="Error verified")

    #Регистарция нового пользователя
    def signup(self,UserEmail:str,UserPassword:str):
        hashUserPassowrd = hash_pass(UserPassword)
        try:
            NewUser = User(email=UserEmail,
                           name="",
                           hashed_password=hashUserPassowrd,
                           is_admin=0,
                           is_active=1)
            self.db.add(NewUser)
            self.db.commit()
        except Exception as e:
            self.db.rollback()
            raise HTTPException(status_code=500, detail="Failed  signup:"+ str(e))
        return {"detail":"signup OK"}