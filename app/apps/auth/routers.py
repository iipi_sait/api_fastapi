from fastapi import APIRouter, Depends, Form
from app.apps.auth.views import AuthView
from app.database import SessionLocal

router = APIRouter()
auth_view = AuthView(db=SessionLocal)


@router.post("/login")
def post_auth_login(data:dict):
    print(data)
    result = auth_view.login(data['email'],data['password'])
    return result

@router.post("/signup")
def post_auth_signup(data:dict):
    result = auth_view.signup(data['email'],data['password'])
    return result
