from fastapi import HTTPException, File, UploadFile
from fastapi.responses import JSONResponse
from datetime import datetime
from sqlalchemy import desc, and_
from sqlalchemy.orm import Session
from passlib.context import CryptContext
from app.models.user import User
from app.models.lang import Lang
from app.models.specialties import  Specialties
from app.models.proffesors import Proffesor
from app.models.categories import Categories
from app.models.news import News
from app.models.htmlblocks import Htmlblocks
from app.models.departgroup import Debartgroup
from app.models.portfolio import Portfolio
import os
import shutil


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
def hash_pass(password:str):
    return pwd_context.hash(password)


class FrontView:

    def __init__(self, db: Session):
        self.db = db

    #Получаем блок
    def getFrontHtmlbloc(self,slug):
        db_record = self.db.query(Htmlblocks).filter(Htmlblocks.slug == slug).first()
        self.db.commit()
        if db_record is None:
            raise HTTPException(status_code=404, detail="not found")
        return {'html':db_record.text}

    def getFrontEducation(self):
        db_records = self.db.query(Specialties).all()
        if db_records is None:
            raise HTTPException(status_code=404, detail="not found")
        resultArray = []
        for val in db_records:
            resultArray.append({"id": val.id,
                                "title": val.name,
                                "slug":val.slug,
                                "direction": val.direction,
                                "profile": val.profile,
                                "period_development": val.period_development,
                                "form_education": val.form_education,
                                "is_dogovor": val.is_pyument,
                                "is_budget": val.is_dogovor,
                                "about": val.about,
                                })
        return resultArray

    def getFrontProffessors(self):
        db_records = self.db.query(Proffesor).all()
        if db_records is None:
            raise HTTPException(status_code=404, detail="not found")
        resultArray = []
        for val in db_records:
            resultArray.append({"id": val.id,
                                "name": val.name,
                                "slug":val.slug,
                                "surname":val.surname,
                                "middle_name": val.middle_name,
                                "photo": val.photo,
                                "post": val.post,
                                "sort": val.sort
                                })
        # Sort the resultArray by the 'sort' attribute
        resultArray = sorted(resultArray, key=lambda x: x['sort'])
        return resultArray
    def getFrontProffessorsSlug(self,slug):
        db_record = self.db.query(Proffesor).filter(Proffesor.slug == slug).first()
        if db_record is None:
            raise HTTPException(status_code=404, detail="not found")    
        resultArray={"id": db_record.id,
                                "name": db_record.name,
                                "slug": db_record.slug,
                                "surname": db_record.surname,
                                "middle_name": db_record.middle_name,
                                "photo": db_record.photo,
                                "post": db_record.post,
                                "sort": db_record.sort,
                                "inf_1": db_record.inf_1,
                                "inf_2": db_record.inf_2,
                                "inf_3": db_record.inf_3,
                                "inf_4": db_record.inf_4,
                                "inf_5": db_record.inf_5,
                                "inf_6": db_record.inf_6,
                                "about_text":db_record.about_text
                                }
        return resultArray

    def getFrontNewCategory(self):
        db_records = self.db.query(Categories).all()
        if db_records is None:
            raise HTTPException(status_code=404, detail="not found")
        resultArray = []
        for val in db_records:
            resultArray.append({"id": val.id,
                                "name": val.name,
                                "slug":val.slug,
                                })
        return resultArray


    def getFrontNews(self, skip:int, limit:int, typecategory:int):
        apply_offset = skip and skip > 0
        # Запрос
        db_records = self.db.query(News)
        if typecategory:
            db_records = db_records.filter(News.categori_id == typecategory)
        db_records = db_records.order_by(News.datapublic.desc())
        if apply_offset:
            offset = (skip - 1) * limit
            db_records = db_records.offset(offset).limit(limit)
        else:
            db_records = db_records.limit(limit)

        # запрос к базе и получаем данные
        db_records=db_records.all()

        #выводим данные
        if db_records is None:
            raise HTTPException(status_code=404, detail="not found")
        resultArray = []
        for val in db_records:
            formatted_date = val.datapublic.strftime('%d-%m-%Y')
            resultArray.append({"id": val.id,
                                "name": val.name,
                                "slug":val.slug,
                                "text": val.text,
                                "categori_id": val.categori_id,
                                "photo": val.photo,
                                "datapublic": formatted_date,
                                })
        return resultArray

    def getFrontNewSlug(self,slug):
        db_record = self.db.query(News).filter(News.slug == slug).first()
        if db_record is None:
            raise HTTPException(status_code=404, detail="not found")
        resultArray = []
        formatted_date = db_record.datapublic.strftime('%d-%m-%Y')
        resultArray.append({"id": db_record.id,
                                "name": db_record.name,
                                "slug":db_record.slug,
                                "text": db_record.text,
                                "text_big": db_record.text_big,
                                "categori_id": db_record.categori_id,
                                "photo": db_record.photo,
                                "datapublic": formatted_date,
                                })
        return resultArray

    def getAccountData(self,user_email):
        groupname=''
        db_user = self.db.query(User).filter(User.email == user_email).first()
        self.db.commit()
        if db_user is None:
            raise HTTPException(status_code=404, detail="User not found")
        db_group = self.db.query(Debartgroup).filter(Debartgroup.id == db_user.departgroupid).first()
        self.db.commit()
        if db_group:
            groupname=db_group.name

        resultArray = {'name':db_user.name,'groupname':groupname,'user_type':db_user.type_user}
        return resultArray

    def getAccountPortfolio(self,user_email):
        groupname=''
        db_user = self.db.query(User).filter(User.email == user_email).first()
        self.db.commit()
        if db_user is None:
            raise HTTPException(status_code=404, detail="User not found")
        db_records = self.db.query(Portfolio).filter(Portfolio.user_id == db_user.id).all()
        self.db.commit()
        resultArray = []
        for val in db_records:
            resultArray.append({"id": val.id,
                                "type": val.type,
                                "vid": val.vid,
                                "name": val.name,
                                "point": val.point,
                                "datarecord": val.datarecord,
                                "about": val.about,
                                })
        resultArray = {'name':db_user.name,'listdatas':resultArray}
        return resultArray

    def getDeletetPortfolio(self,user_email,idrecord):
        groupname=''
        db_user = self.db.query(User).filter(User.email == user_email).first()
        self.db.commit()
        if db_user is None:
            raise HTTPException(status_code=404, detail="User not found")
        db_delete = self.db.query(Portfolio).filter(Portfolio.id == idrecord).delete()
        self.db.commit()
        db_records = self.db.query(Portfolio).filter(Portfolio.user_id == db_user.id).all()
        self.db.commit()
        resultArray = []
        for val in db_records:
            resultArray.append({"id": val.id,
                                "type": val.type,
                                "vid": val.vid,
                                "name": val.name,
                                "point": val.point,
                                "datarecord": val.datarecord,
                                "about": val.about,
                                })
        resultArray = {'name':db_user.name,'listdatas':resultArray}
        return resultArray

    def setAccountData(self,datas:dict,user_email):
        db_user = self.db.query(User).filter(User.email == user_email).first()
        self.db.commit()
        if db_user is None:
            raise HTTPException(status_code=404, detail="User not found")
        objModel = Portfolio
        try:
            datas['user_id'] = db_user.id
            NewRecorcd = objModel(**datas)
            self.db.add(NewRecorcd)
            self.db.commit()
            self.db.refresh(NewRecorcd)
        except Exception as e:
            self.db.rollback()
            raise HTTPException(status_code=500, detail="Failed  Table:portfolio Msg :"+ str(e))
        return NewRecorcd

    def getGroupList(self,user_email):
        db_user = self.db.query(User).filter(User.email == user_email).first()
        self.db.commit()
        if db_user is None:
            raise HTTPException(status_code=404, detail="User not found")
        if db_user.type_user == 2  :
            raise HTTPException(status_code=404, detail="User not access")

        if db_user.type_user==3:
            db_records = self.db.query(Debartgroup).all()

        if db_user.type_user==1:

            db_records = self.db.query(Debartgroup).filter(Debartgroup.id == db_user.departgroupid).all()
        self.db.commit()

        resultArray = []
        for val in db_records:
            resultArray.append({"id": val.id,"name": val.name})
        return resultArray



    def setNewPassword(self, datas: dict, user_email):
        db_user = self.db.query(User).filter(User.email == user_email).first()
        self.db.commit()
        if db_user is None:
            raise HTTPException(status_code=404, detail="User not found")
        UserPassword=datas.get('oldPassword')
        is_verified = pwd_context.verify(UserPassword, db_user.hashed_password)
        if is_verified:
            print('1dsadassdsa')
            if db_user:
                # Hash the new password
                hashUserPassowrd = hash_pass(datas.get('newPassword'))
                # Update the user's password
                db_user.hashed_password = hashUserPassowrd
                # Commit the changes to the database
                self.db.commit()
        else:
            print('dsadassd')
            raise HTTPException(status_code=404, detail="is_verified error")

    def getReportPorfolio(self,datas:dict,user_email):

        db_user = self.db.query(User).filter(User.email == user_email).first()
        self.db.commit()
        if db_user is None:
            raise HTTPException(status_code=404, detail="User not found")
        if db_user.type_user == 2:
            raise HTTPException(status_code=404, detail="User not access")


        dataend=datas.get('dataend')
        datastart=datas.get('datastart')
        group=datas.get('group')

        # получаем массив пользователей
        resultArray = []
        if (datastart == "" and dataend != ""):
            db_records = self.db.query(Portfolio.user_id).filter(Portfolio.datarecord<=dataend).group_by(Portfolio.user_id).all()
        if (datastart != "" and dataend != ""):
            db_records = self.db.query(Portfolio.user_id).filter(
            and_(
                Portfolio.datarecord >= datastart,
                Portfolio.datarecord <= dataend
            )
        ).group_by(Portfolio.user_id).all()
        if (datastart == "" and dataend == ""):
            db_records = self.db.query(Portfolio.user_id).group_by(Portfolio.user_id).all()
        if (datastart != "" and dataend == ""):
            db_records = self.db.query(Portfolio.user_id).filter(Portfolio.datarecord>=datastart).group_by(Portfolio.user_id).all()

        self.db.commit()
        resultArrayUsers = []
        for val in db_records:
            resultArrayUsers.append(val.user_id)

        # получаем группы которые есть
        if group!="0":
            db_records = self.db.query(User.departgroupid).filter(User.id.in_(resultArrayUsers),User.departgroupid == group).all()
        else:
            db_records = self.db.query(User.departgroupid).filter(User.id.in_(resultArrayUsers)).all()

        self.db.commit()
        resultArrayGroups = []
        for val in db_records:
            resultArrayGroups.append(val.departgroupid)


        for groupid in resultArrayGroups:
            db_group= self.db.query(Debartgroup).filter(Debartgroup.id == groupid).first()
            self.db.commit()

            data_users = self.db.query(User).filter(User.departgroupid == groupid).all()
            self.db.commit()
            user_list=[]
            for data_user in data_users:

                if (datastart == "" and dataend != ""):
                    data_portfolios = (self.db.query(Portfolio)
                                       .filter(Portfolio.user_id == data_user.id,Portfolio.datarecord <=  dataend)
                                       .order_by(Portfolio.type.asc(), Portfolio.datarecord.desc())
                                       .all())
                if (datastart != "" and dataend != ""):
                    data_portfolios = (self.db.query(Portfolio)
                                       .filter(Portfolio.user_id == data_user.id, and_(
                                            Portfolio.datarecord >= datastart,
                                            Portfolio.datarecord <= dataend
                                        ))
                                       .order_by(Portfolio.type.asc(), Portfolio.datarecord.desc())
                                       .all())
                if (datastart == "" and dataend == ""):
                    data_portfolios = (self.db.query(Portfolio)
                                       .filter(Portfolio.user_id == data_user.id)
                                       .order_by(Portfolio.type.asc(), Portfolio.datarecord.desc())
                                       .all())
                if (datastart != "" and dataend == ""):
                    data_portfolios = (self.db.query(Portfolio)
                                       .filter(Portfolio.user_id == data_user.id,Portfolio.datarecord >= datastart)
                                       .order_by(Portfolio.type.asc(), Portfolio.datarecord.desc())
                                       .all())



                self.db.commit()
                portf_list=[]
                for data_portfolio in data_portfolios:
                    portf_list.append({"id":data_portfolio.id,
                                       "type":data_portfolio.type,
                                       "vid":data_portfolio.vid,
                                       "name":data_portfolio.name,
                                       "point": data_portfolio.point,
                                       "datarecord": data_portfolio.datarecord,
                                       "about": data_portfolio.about,
                                       })

                user_list.append({"id":data_user.id,"name":data_user.name,"datas":portf_list})
            resultArray.append({'groupName':db_group.name, 'datauser':user_list})

        return resultArray