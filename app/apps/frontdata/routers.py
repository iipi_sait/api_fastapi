from fastapi import APIRouter,Depends
from fastapi.security import OAuth2PasswordBearer
from app.apps.frontdata.views import FrontView
from app.database import SessionLocal
from app.apps.auth.views import SECRET_KEY,ALGORITHM
from jose import jwt

router = APIRouter()
front_view=FrontView(db=SessionLocal)
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

async def get_user_from_token(token: str = Depends(oauth2_scheme)):
    try:
       payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])  # декодируем токен
       return payload.get("sub")
    except jwt.ExpiredSignatureError:
       pass
    except jwt.InvalidTokenError:
       pass



@router.get("/htmlblock/{slug}")
async def get_block_html(slug: str):
    datarecord=front_view.getFrontHtmlbloc(slug)
    return datarecord

@router.get("/educations")
async def get_list_educations():
    datarecord=front_view.getFrontEducation()
    return datarecord

@router.get("/proffessors")
async def get_list_educations():
    datarecord=front_view.getFrontProffessors()
    return datarecord

@router.get("/proffessors/{slug}")
async def get_list_educations(slug):
    datarecord=front_view.getFrontProffessorsSlug(slug)
    return datarecord



@router.get("/newcategories/")
async def get_list_category():
    datarecord=front_view.getFrontNewCategory()
    return datarecord

@router.get("/news/")
async def get_list_news(skip: int = 0, limit: int = 9, typecategory: int = 0):
    datarecord=front_view.getFrontNews(skip,limit,typecategory)
    return datarecord

@router.get("/news/{slug}")
async def get_list_news(slug):
    datarecord=front_view.getFrontNewSlug(slug)
    return datarecord

@router.post("/account/data")
async def crud_show_datas(current_user: str = Depends(get_user_from_token)):
    datas=front_view.getAccountData(current_user)
    return {"dataaccount":datas}

@router.post("/account/setdata")
async def crud_show_datas(data:dict,current_user: str = Depends(get_user_from_token)):
    datas=front_view.setAccountData(data,current_user)
    return {"result":datas}

@router.post("/grouplist")
async def list_group(current_user: str = Depends(get_user_from_token)):
    datas=front_view.getGroupList(current_user)
    return {"result":datas}

@router.post("/account/portfolio")
async def crud_show_datas(current_user: str = Depends(get_user_from_token)):
    datas=front_view.getAccountPortfolio(current_user)
    return {"dataportfolio":datas}


#Удаление
@router.post("/portfolio/delete")
async def delete_portfolio_datas(data:dict,current_user: str = Depends(get_user_from_token)):
    datas=front_view.getDeletetPortfolio(current_user,data.get('idrecord'))
    return {"dataportfolio":datas}


@router.post("/changepassword/")
async def change_password(data:dict,current_user: str = Depends(get_user_from_token)):
    front_view.setNewPassword(data,current_user)
    return {"change":'ok'}


#Отчет 
@router.post("/report/portfolio")
async def crud_show_datas(data:dict,current_user: str = Depends(get_user_from_token)):
    datas=front_view.getReportPorfolio(data,current_user)
    return {"reports":datas}

