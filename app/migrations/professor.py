import sqlalchemy


metadata = sqlalchemy.MetaData()


professors_table = sqlalchemy.Table(
    "professors",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String(100)),
    sqlalchemy.Column("surname", sqlalchemy.String(100)),
    sqlalchemy.Column("middle_name", sqlalchemy.String(100)),
    sqlalchemy.Column("slug", sqlalchemy.String(200), unique=True, index=True),
    sqlalchemy.Column("post", sqlalchemy.String(100)),
    sqlalchemy.Column("photo", sqlalchemy.String(100),nullable=True),
    sqlalchemy.Column("sort", sqlalchemy.Integer()),
    sqlalchemy.Column("about", sqlalchemy.Text(),nullable=True),
    sqlalchemy.Column("inf_1", sqlalchemy.Text(),nullable=True),
    sqlalchemy.Column("inf_2", sqlalchemy.Text(),nullable=True),
    sqlalchemy.Column("inf_3", sqlalchemy.Text(),nullable=True),
    sqlalchemy.Column("inf_4", sqlalchemy.Text(),nullable=True),
    sqlalchemy.Column("inf_5", sqlalchemy.Text(),nullable=True),
    sqlalchemy.Column("inf_6", sqlalchemy.Text(),nullable=True),
    sqlalchemy.Column("about_text", sqlalchemy.Text(),nullable=True),
)
