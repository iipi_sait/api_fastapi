import sqlalchemy


metadata = sqlalchemy.MetaData()


htmlblock_table = sqlalchemy.Table(
    "htmlblocks",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String(100), unique=True, index=True),
    sqlalchemy.Column("slug", sqlalchemy.String(100), unique=True, index=True),
    sqlalchemy.Column("text", sqlalchemy.Text()),

)
