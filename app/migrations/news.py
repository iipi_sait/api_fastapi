import sqlalchemy
from .categories import categori_table

metadata = sqlalchemy.MetaData()


new_table = sqlalchemy.Table(
    "news",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String(100), unique=True),
    sqlalchemy.Column("slug", sqlalchemy.String(200), unique=True, index=True),
    sqlalchemy.Column("text", sqlalchemy.Text()),
    sqlalchemy.Column("categori_id",
                      sqlalchemy.ForeignKey(categori_table.c.id, ondelete="CASCADE"),
                      nullable=True),
    sqlalchemy.Column("photo", sqlalchemy.String(100),nullable=True),
    sqlalchemy.Column("datapublic", sqlalchemy.DateTime,nullable=True),
    sqlalchemy.Column("text_big", sqlalchemy.Text(),nullable=True),
)
