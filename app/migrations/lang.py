import sqlalchemy

metadata = sqlalchemy.MetaData()

lang_table = sqlalchemy.Table(
    "langs",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("short", sqlalchemy.String(2), unique=True, index=True),
    sqlalchemy.Column("name", sqlalchemy.String(100)),
)
