from sqlalchemy import DateTime

import sqlalchemy
import datetime

metadata = sqlalchemy.MetaData()

portfolio_table = sqlalchemy.Table(
    "portfolio",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("user_id", sqlalchemy.Integer),
    sqlalchemy.Column("type", sqlalchemy.Integer),
    sqlalchemy.Column("vid", sqlalchemy.String(100),nullable=True,   index=True),
    sqlalchemy.Column("name", sqlalchemy.Text(),nullable=False, index=True),
    sqlalchemy.Column("point", sqlalchemy.String(100),nullable=False,  index=True),
    sqlalchemy.Column(
        "datarecord",
        DateTime,
        nullable=False,
        index=True
    ),
    sqlalchemy.Column("about", sqlalchemy.Text(),nullable=True)
)
