import sqlalchemy


metadata = sqlalchemy.MetaData()

specialti_table = sqlalchemy.Table(
    "specialties",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String(100), unique=True, index=True),
    sqlalchemy.Column("slug", sqlalchemy.String(100), unique=True, index=True),
    sqlalchemy.Column("direction", sqlalchemy.String(240), index=True),
    sqlalchemy.Column("profile", sqlalchemy.String(240),  index=True),
    sqlalchemy.Column("period_development", sqlalchemy.String(100)),
    sqlalchemy.Column("form_education", sqlalchemy.String(100)),
    sqlalchemy.Column(
            "is_dogovor",
            sqlalchemy.Boolean(),
            server_default=sqlalchemy.sql.expression.true(),
            nullable=True,
        ),
    sqlalchemy.Column(
            "is_pyument",
            sqlalchemy.Boolean(),
            server_default=sqlalchemy.sql.expression.true(),
            nullable=True,
        ),
    sqlalchemy.Column("about", sqlalchemy.Text(),nullable=True)

)
