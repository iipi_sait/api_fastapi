import sqlalchemy

metadata = sqlalchemy.MetaData()

departgroup_table = sqlalchemy.Table(
    "departgroup",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String(100), unique=True, index=True),
    sqlalchemy.Column("about", sqlalchemy.Text())
)
