import sqlalchemy

metadata = sqlalchemy.MetaData()

categori_table = sqlalchemy.Table(
    "categories",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("slug", sqlalchemy.String(100), unique=True, index=True),
    sqlalchemy.Column("name", sqlalchemy.String(100)),
)
