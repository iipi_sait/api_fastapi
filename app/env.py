from logging.config import fileConfig

from alembic import context
from sqlalchemy import engine_from_config
from sqlalchemy import pool

# Импортируйте MetaData
from sqlalchemy import MetaData

# Этот код отвечает за настройку логирования. Не изменяйте его.
config = context.config
fileConfig(config.config_file_name)

# Создайте объект MetaData
target_metadata = MetaData()

# Этот код отвечает за создание соединения с базой данных
# Оставьте его без изменений
def run_migrations_offline():
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()

# Этот код отвечает за создание соединения с базой данных
# Оставьте его без изменений
def run_migrations_online():
    connectable = engine_from_config(
        config.get_section(config.config_ini_section),
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )
    with connectable.connect() as connection:
        context.configure(
            connection=connection, target_metadata=target_metadata
        )

        with context.begin_transaction():
            context.run_migrations()

# Здесь выбирается способ запуска миграций, он может быть online или offline.
# Выберите соответствующий метод в зависимости от вашей конфигурации.
if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
