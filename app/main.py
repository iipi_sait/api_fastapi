from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from app.apps.auth.routers import router as auth_router
from app.apps.crud.routers import router as crud_router
from app.apps.frontdata.routers import router as front_router


# Создание экземпляра FastAPI
app = FastAPI(
    title="Back Api IIPI",
    description="Description",
    version="1.0.0",
)

app.mount("/images", StaticFiles(directory="/code/app/images"), name="images")

### START CONFIG CORS
origins = [
    "http://127.0.0.1",
    "http://127.0.0.1:3000",
    "http://127.0.0.1:3001",
    "http://127.0.0.1:8022",
    "http://localhost:3000",
    "http://127.0.0.1:8523",
    "http://localhost:8022",
    "http://10.1.0.47:8104",
    "http://10.1.0.47:3000",
    "https://adminiipi.rfpgu.ru",
    "http://adminrmebel.rfpgu.ru",
    "https://adminiipi.rfpgu.ru",
    "https://frontiipi.rfpgu.ru",
    "https://ipi.rfpgu.ru"
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
### END CONFIG CORS


# Подключение роутеров из каждого приложения
app.include_router(auth_router, prefix="/auth")
app.include_router(crud_router, prefix="/crud")
app.include_router(front_router, prefix="/api")