# Описание проекта
Api сервер для сайта

# Установка проекта
docker-compose up --d

# Запуск проекта
docker-compose up

# Миграция проекта
alembic revision --autogenerate -m "create table user and lang"
alembic upgrade head