FROM python:3.12
#
WORKDIR /code
#
COPY ./requirements.txt /code/app/requirements.txt
#
RUN python3 -m pip config --user set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
RUN python3 -m pip config --user set global.timeout 150
RUN pip install --upgrade pip
RUN pip install --no-cache-dir --upgrade -r /code/app/requirements.txt
RUN pip install fastapi uvicorn databases[asyncpg]
RUN pip install alembic
RUN pip install psycopg2-binary
RUN pip install --upgrade pip
RUN pip install passlib
RUN pip install python-jose>=3.0.0
RUN pip install aiofiles
RUN pip install python-multipart
RUN pip install pytest
RUN pip install requests
RUN pip install pytest-asyncio
RUN pip install asyncpg
RUN pip install python-dotenv
#
COPY ./app /code/app
#
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80","--reload"]
